<?php
/**
 * Plugin Name: WooCommerce Memberships
 * Plugin URI: https://www.woocommerce.com/products/woocommerce-memberships/
 * Description: Sell memberships that provide access to restricted content, products, discounts, and more!
 * Author: SkyVerge
 * Author URI: https://www.woocommerce.com/
 * Version: 1.10.6
 * Text Domain: woocommerce-memberships
 * Domain Path: /i18n/languages/
 *
 * Copyright: (c) 2014-2018 SkyVerge, Inc. (info@skyverge.com)
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 * @package   Memberships
 * @author    SkyVerge
 * @copyright Copyright (c) 2014-2018, SkyVerge, Inc.
 * @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 *
 * Woo: 958589:9288e7609ad0b487b81ef6232efa5cfc
 * WC requires at least: 2.6.14
 * WC tested up to: 3.4.3
 */

defined( 'ABSPATH' ) or exit;

// Required functions
if ( ! function_exists( 'woothemes_queue_update' ) ) {
	require_once( plugin_dir_path( __FILE__ ) . 'woo-includes/woo-functions.php' );
}

// Plugin updates
woothemes_queue_update( plugin_basename( __FILE__ ), '9288e7609ad0b487b81ef6232efa5cfc', '958589' );

// WC active check
if ( ! is_woocommerce_active() ) {
	return;
}

// Required library class
if ( ! class_exists( 'SV_WC_Framework_Bootstrap' ) ) {
	require_once( plugin_dir_path( __FILE__ ) . 'lib/skyverge/woocommerce/class-sv-wc-framework-bootstrap.php' );
}

SV_WC_Framework_Bootstrap::instance()->register_plugin( '4.9.1', __( 'WooCommerce Memberships', 'woocommerce-memberships' ), __FILE__, 'init_woocommerce_memberships', array(
	'minimum_wc_version'   => '2.6.14',
	'minimum_wp_version'   => '4.4',
	'backwards_compatible' => '4.4.0',
) );

// Required Action Scheduler library
require_once( plugin_dir_path( __FILE__ ) . 'lib/prospress/action-scheduler/action-scheduler.php' );

function init_woocommerce_memberships() {


/**
 * WooCommerce Memberships Main Plugin Class.
 *
 * @since 1.0.0
 */
class WC_Memberships extends SV_WC_Plugin {


	/** plugin version number */
	const VERSION = '1.10.6';

	/** @var WC_Memberships single instance of this plugin */
	protected static $instance;

	/** plugin id */
	const PLUGIN_ID = 'memberships';

	/** @var \WC_Memberships_Admin instance */
	protected $admin;

	/** @var \WC_Memberships_AJAX instance */
	protected $ajax;

	/** @var \WC_Memberships_Capabilities instance */
	protected $capabilities;

	/** @var \WC_Memberships_Emails instance */
	protected $emails;

	/** @var \WC_Memberships_Frontend instance */
	protected $frontend;

	/** @var WC_Memberships_Integrations instance */
	protected $integrations;

	/** @var \WC_Memberships_Member_Discounts instance */
	protected $member_discounts;

	/** @var \WC_Memberships_Membership_Plans instance */
	protected $plans;

	/** @var \WC_Memberships_Restrictions instance */
	protected $restrictions;

	/** @var \WC_Memberships_Shipping instance */
	private $shipping;

	/** @var \WC_Memberships_Rules instance */
	protected $rules;

	/** @var \WC_Memberships_User_Memberships instance */
	protected $user_memberships;

	/** @var \WC_Memberships_Utilities instance */
	private $utilities;


	/**
	 * Initializes the plugin.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		parent::__construct(
			self::PLUGIN_ID,
			self::VERSION,
			array(
				'text_domain'        => 'woocommerce-memberships',
				'display_php_notice' => true,
				'dependencies'       => array(
					'mbstring',
				),
			)
		);

		// include required files
		add_action( 'sv_wc_framework_plugins_loaded', array( $this, 'includes' ) );

		// initialize
		add_action( 'init', array( $this, 'init' ) );

		// make sure template files are searched for in our plugin
		add_filter( 'woocommerce_locate_template',      array( $this, 'locate_template' ), 20, 3 );
		add_filter( 'woocommerce_locate_core_template', array( $this, 'locate_template' ), 20, 3 );

		// lifecycle
		add_action( 'admin_init', array ( $this, 'maybe_activate' ) );
		register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );

		// add query vars for rewrite endpoints
		add_filter( 'query_vars', array( $this, 'add_query_vars' ), 0 );

		// GDPR handling: remove user memberships when erasing personal data
		add_filter( 'wp_privacy_personal_data_erasers',   array( $this, 'register_personal_data_eraser' ) );
		// GDPR handling: export user memberships personal data upon request
		add_filter( 'wp_privacy_personal_data_exporters', array( $this, 'register_personal_data_exporter' ), 6 );
	}


	/**
	 * Includes required files.
	 *
	 * @internal
	 *
	 * @since 1.0.0
	 */
	public function includes() {

		// load post types
		require_once( $this->get_plugin_path() . '/includes/class-wc-memberships-post-types.php' );

		// load user messages helper
		require_once( $this->get_plugin_path() . '/includes/class-wc-memberships-user-messages.php' );

		// load helper functions
		require_once( $this->get_plugin_path() . '/includes/functions/wc-memberships-functions.php' );

		// init general classes
		$this->rules            = $this->load_class( '/includes/class-wc-memberships-rules.php',            'WC_Memberships_Rules' );
		$this->plans            = $this->load_class( '/includes/class-wc-memberships-membership-plans.php', 'WC_Memberships_Membership_Plans' );
		$this->emails           = $this->load_class( '/includes/class-wc-memberships-emails.php',           'WC_Memberships_Emails' );
		$this->user_memberships = $this->load_class( '/includes/class-wc-memberships-user-memberships.php', 'WC_Memberships_User_Memberships' );
		$this->capabilities     = $this->load_class( '/includes/class-wc-memberships-capabilities.php',     'WC_Memberships_Capabilities' );
		$this->member_discounts = $this->load_class( '/includes/class-wc-memberships-member-discounts.php', 'WC_Memberships_Member_Discounts' );
		$this->restrictions     = $this->load_class( '/includes/class-wc-memberships-restrictions.php',     'WC_Memberships_Restrictions' );
		$this->shipping         = $this->load_class( '/includes/class-wc-memberships-shipping.php',         'WC_Memberships_Shipping' );

		// frontend includes
		if ( ! is_admin() ) {
			$this->frontend_includes();
		}

		// admin includes
		if ( is_admin() && ! is_ajax() ) {
			$this->admin_includes();
		}

		// AJAX includes
		if ( is_ajax() ) {
			$this->ajax_includes();
		}

		// load utilities
		$this->utilities = $this->load_class( '/includes/class-wc-memberships-utilities.php', 'WC_Memberships_Utilities' );

		// load integrations
		$this->integrations = $this->load_class( '/includes/integrations/class-wc-memberships-integrations.php', 'WC_Memberships_Integrations' );

		// WP CLI support
		if ( defined( 'WP_CLI' ) && WP_CLI && version_compare( PHP_VERSION, '5.3.0', '>=' ) ) {
			include_once $this->get_plugin_path() . '/includes/class-wc-memberships-cli.php';
		}
	}


	/**
	 * Includes required admin classes.
	 *
	 * @since 1.0.0
	 */
	private function admin_includes() {

		$this->admin = $this->load_class( '/includes/admin/class-wc-memberships-admin.php', 'WC_Memberships_Admin' );

		// message handler
		$this->admin->message_handler = $this->get_message_handler();
	}


	/**
	 * Includes required AJAX classes.
	 *
	 * @since 1.0.0
	 */
	private function ajax_includes() {

		$this->ajax = $this->load_class( '/includes/class-wc-memberships-ajax.php', 'WC_Memberships_AJAX' );
	}


	/**
	 * Includes required frontend classes.
	 *
	 * @since 1.0.0
	 */
	private function frontend_includes() {

		// init shortcodes
		require_once( $this->get_plugin_path() . '/includes/class-wc-memberships-shortcodes.php' );
		WC_Memberships_Shortcodes::initialize();

		// load front end
		$this->frontend = $this->load_class( '/includes/frontend/class-wc-memberships-frontend.php', 'WC_Memberships_Frontend' );
	}


	/**
	 * Returns the Admin instance.
	 *
	 * @since 1.6.0
	 *
	 * @return \WC_Memberships_Admin
	 */
	public function get_admin_instance() {
		return $this->admin;
	}


	/**
	 * Returns the AJAX instance.
	 *
	 * @since 1.6.0
	 *
	 * @return \WC_Memberships_AJAX
	 */
	public function get_ajax_instance() {
		return $this->ajax;
	}


	/**
	 * Returns the Capabilities instance.
	 *
	 * @since 1.6.0
	 *
	 * @return \WC_Memberships_Capabilities
	 */
	public function get_capabilities_instance() {
		return $this->capabilities;
	}


	/**
	 * Get the Restrictions instance.
	 *
	 * @since 1.9.0
	 *
	 * @return \WC_Memberships_Restrictions
	 */
	public function get_restrictions_instance() {
		return $this->restrictions;
	}


	/**
	 * Returns the Shipping handler instance.
	 *
	 * @since 1.10.0
	 *
	 * @return \WC_Memberships_Shipping
	 */
	public function get_shipping_instance() {
		return $this->shipping;
	}


	/**
	 * Returns the Frontend instance.
	 *
	 * @since 1.6.0
	 *
	 * @return \WC_Memberships_Frontend
	 */
	public function get_frontend_instance() {
		return $this->frontend;
	}


	/**
	 * Returns the Emails instance.
	 *
	 * @since 1.6.0
	 *
	 * @return \WC_Memberships_Emails
	 */
	public function get_emails_instance() {
		return $this->emails;
	}


	/**
	 * Returns the Integrations instance.
	 *
	 * @since 1.6.0
	 *
	 * @return \WC_Memberships_Integrations
	 */
	public function get_integrations_instance() {
		return $this->integrations;
	}


	/**
	 * Returns the Member Discounts instance.
	 *
	 * @since 1.6.0
	 *
	 * @return \WC_Memberships_Member_Discounts
	 */
	public function get_member_discounts_instance() {
		return $this->member_discounts;
	}


	/**
	 * Returns the Membership Plans instance.
	 *
	 * @since 1.6.0
	 *
	 * @return \WC_Memberships_Membership_Plans
	 */
	public function get_plans_instance() {
		return $this->plans;
	}


	/**
	 * Returns the Rules instance.
	 *
	 * @since 1.6.0
	 *
	 * @return \WC_Memberships_Rules
	 */
	public function get_rules_instance() {
		return $this->rules;
	}


	/**
	 * Returns the User Memberships instance.
	 *
	 * @since 1.6.0
	 *
	 * @return \WC_Memberships_User_Memberships
	 */
	public function get_user_memberships_instance() {
		return $this->user_memberships;
	}


	/**
	 * Returns the utilities handler instance.
	 *
	 * @since 1.10.0
	 *
	 * @return \WC_Memberships_Utilities
	 */
	public function get_utilities_instance() {
		return $this->utilities;
	}


	/**
	 * Initializes post types.
	 *
	 * @since 1.0.0
	 */
	public function init() {

		WC_Memberships_Post_Types::initialize();

		$this->add_rewrite_endpoints();
	}


	/**
	 * Locates the WooCommerce template files from our templates directory.
	 *
	 * @internal
	 *
	 * @since 1.0.0
	 *
	 * @param string $template already found template
	 * @param string $template_name searchable template name
	 * @param string $template_path template path
	 * @return string search result for the template
	 */
	public function locate_template( $template, $template_name, $template_path ) {

		// only keep looking if no custom theme template was found
		// or if a default WooCommerce template was found
		if ( ! $template || SV_WC_Helper::str_starts_with( $template, WC()->plugin_path() ) ) {

			// set the path to our templates directory
			$plugin_path = $this->get_plugin_path() . '/templates/';

			// if a template is found, make it so
			if ( is_readable( $plugin_path . $template_name ) ) {
				$template = $plugin_path . $template_name;
			}
		}

		return $template;
	}


	/** Admin methods ******************************************************/


	/**
	 * Renders a notice for the user to read the docs before adding add-ons.
	 *
	 * @see \SV_WC_Plugin::add_admin_notices()
	 *
	 * @since 1.0.0
	 */
	public function add_admin_notices() {

		// show any dependency notices
		parent::add_admin_notices();

		$screen = get_current_screen();

		// only render on plugins or settings screen
		if ( 'plugins' === $screen->id || $this->is_plugin_settings() ) {

			$this->get_admin_notice_handler()->add_admin_notice(
				/* translators: the %s placeholders are meant for pairs of opening <a> and closing </a> link tags */
				sprintf( __( 'Thanks for installing Memberships! To get started, take a minute to %1$sread the documentation%2$s and then %3$ssetup a membership plan%4$s :)', 'woocommerce-memberships' ),
					'<a href="https://docs.woocommerce.com/document/woocommerce-memberships/" target="_blank">',
					'</a>',
					'<a href="' . admin_url( 'edit.php?post_type=wc_membership_plan' ) . '">',
					'</a>' ),
				'get-started-notice',
				array( 'always_show_on_settings' => false, 'notice_class' => 'updated' )
			);
		}
	}


	/** Helper methods ******************************************************/


	/**
	 * Returns the Memberships instance singleton.
	 *
	 * Ensures only one instance is/can be loaded.
	 * @see wc_memberships()
	 *
	 * @since 1.0.0
	 *
	 * @return \WC_Memberships
	 */
	public static function instance() {
		if ( null === self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}


	/**
	 * Returns the plugin documentation URL.
	 *
	 * @see \SV_WC_Plugin::get_documentation_url()
	 *
	 * @since 1.2.0
	 *
	 * @return string URL
	 */
	public function get_documentation_url() {
		return 'https://docs.woocommerce.com/document/woocommerce-memberships/';
	}


	/**
	 * Returns the plugin support URL.
	 *
	 * @see \SV_WC_Plugin::get_support_url()
	 *
	 * @since 1.2.0
	 *
	 * @return string URL
	 */
	public function get_support_url() {
		return 'https://woocommerce.com/my-account/tickets/';
	}


	/**
	 * Returns the plugin name, localized.
	 *
	 * @see \SV_WC_Plugin::get_plugin_name()
	 *
	 * @since 1.0.0
	 *
	 * @return string
	 */
	public function get_plugin_name() {
		return __( 'WooCommerce Memberships', 'woocommerce-memberships' );
	}


	/**
	 * Returns the plugin filename path.
	 *
	 * @see \SV_WC_Plugin::get_file()
	 *
	 * @since 1.0.0
	 *
	 * @return string the full path and filename of the plugin file
	 */
	protected function get_file() {
		return __FILE__;
	}


	/**
	 * Checks if the current is the Memberships Settings page.
	 *
	 * @see \SV_WC_Plugin::is_plugin_settings()
	 *
	 * @since 1.0.0
	 *
	 * @return bool
	 */
	public function is_plugin_settings() {

		return isset( $_GET['page'] ) && 'wc-settings' === $_GET['page'] && isset( $_GET['tab'] )
		       // the plugin's main settings page
		       && ( 'memberships' === $_GET['tab']
		       // the plugin's email settings pages
		       || ( 'email' === $_GET['tab'] && isset( $_GET['section'] ) && SV_WC_Helper::str_starts_with( $_GET['section'], 'wc_memberships' ) ) );
	}


	/**
	 * Returns the plugin configuration URL.
	 *
	 * @see \SV_WC_Plugin::get_settings_link()
	 *
	 * @since 1.0.0
	 *
	 * @param string $plugin_id optional plugin identifier
	 * @return string plugin settings URL
	 */
	public function get_settings_url( $plugin_id = null ) {
		return admin_url( 'admin.php?page=wc-settings&tab=memberships' );
	}


	/** Lifecycle methods ******************************************************/


	/**
	 * Installs default settings & pages.
	 *
	 * @see \SV_WC_Plugin::install()
	 *
	 * @since 1.0.0
	 */
	protected function install() {

		// install default "content restricted" page
		$title   = _x( 'Content restricted', 'Page title', 'woocommerce-memberships' );
		$slug    = _x( 'content-restricted', 'Page slug', 'woocommerce-memberships' );
		$content = '[wcm_content_restricted]';

		wc_create_page( esc_sql( $slug ), 'wc_memberships_redirect_page_id', $title, $content );

		// show a notice about restricted content to admin users as they get started
		update_option( 'wc_memberships_admin_restricted_content_notice', 'yes' );

		// include settings so we can install defaults
		include_once( WC()->plugin_path() . '/includes/admin/settings/class-wc-settings-page.php' );
		$settings = $this->load_class( '/includes/admin/class-wc-memberships-settings.php', 'WC_Settings_Memberships' );

		// install default settings for each section
		foreach ( $settings->get_sections() as $section => $label ) {

			foreach ( $settings->get_settings( $section ) as $setting ) {

				if ( isset( $setting['id'], $setting['default'] ) ) {

					update_option( $setting['id'], $setting['default'] );
				}
			}
		}
	}


	/**
	 * Runs upgrade scripts.
	 *
	 * @see \SV_WC_Plugin::install()
	 *
	 * @since 1.1.0
	 *
	 * @param string $installed_version semver
	 */
	protected function upgrade( $installed_version ) {

		require_once( $this->get_plugin_path() . '/includes/class-wc-memberships-upgrade.php' );

		WC_Memberships_Upgrade::run_update_scripts( $installed_version );

		$this->add_rewrite_endpoints();

		flush_rewrite_rules();
	}


	/**
	 * Handles plugin activation.
	 *
	 * @internal
	 *
	 * @since 1.0.0
	 */
	public function maybe_activate() {

		$is_active = get_option( 'wc_memberships_is_active', false );

		if ( ! $is_active ) {

			update_option( 'wc_memberships_is_active', true );

			/**
			 * Runs when Memberships is activated.
			 *
			 * @since 1.0.0
			 */
			do_action( 'wc_memberships_activated' );

			$this->add_rewrite_endpoints();

			flush_rewrite_rules();
		}
	}


	/**
	 * Handles plugin deactivation.
	 *
	 * @internal
	 *
	 * @since 1.0.0
	 */
	public function deactivate() {

		delete_option( 'wc_memberships_is_active' );

		/**
		 * Runs when Memberships is deactivated.
		 *
		 * @since 1.0.0
		 */
		do_action( 'wc_memberships_deactivated' );

		flush_rewrite_rules();
	}


	/**
	 * Adds rewrite rules endpoints.
	 *
	 * @see \WC_Query::get_query_vars()
	 * @see \WC_Query::add_endpoints()
	 *
	 * @since 1.9.0
	 */
	private function add_rewrite_endpoints() {

		// add Members Area endpoint
		add_rewrite_endpoint( get_option( 'woocommerce_myaccount_members_area_endpoint', 'members-area' ), EP_PAGES );
	}


	/**
	 * Handles query vars for endpoints.
	 *
	 * @see \WC_Query::get_query_vars()
	 * @see \WC_Query::add_endpoints()
	 *
	 * @internal
	 *
	 * @since 1.9.0
	 *
	 * @param array $query_vars associative array
	 * @return array
	 */
	public function add_query_vars( $query_vars ) {

		$query_vars[] = get_option( 'using_permalinks' ) ? get_option( 'woocommerce_myaccount_members_area_endpoint', 'members-area' ) : 'members_area';

		return $query_vars;
	}


	/** Privacy methods ******************************************************/


	/**
	 * Registers a GDPR compliant personal data eraser in WordPress for handling User Memberships.
	 *
	 * @internal
	 *
	 * @since 1.10.3
	 *
	 * @param array $erasers list of WordPress personal data erasers
	 * @return array
	 */
	public function register_personal_data_eraser( array $erasers ) {

		$erasers['woocommerce-memberships-user-memberships'] = array(
			'eraser_friendly_name' => __( 'User Memberships', 'woocommerce-memberships' ),
			'callback'               => array( 'WC_Memberships', 'erase_personal_data' ),
		);

		return $erasers;
	}


	/**
	 * Erases personal data from a user membership when an erasure request is issued in WordPress.
	 *
	 * @internal
	 *
	 * @since 1.10.3
	 *
	 * @param string $email_address address of the user that issued the erasure request
	 * @return array associative array with erasure response
	 */
	public static function erase_personal_data( $email_address ) {

		$response = array(
			'items_removed'  => false,
			'items_retained' => false,
			'messages'       => array(),
			'done'           => true,
		);

		if ( is_string( $email_address ) ) {

			$user             = get_user_by( 'email', trim( $email_address ) );
			$user_memberships = $user ? wc_memberships_get_user_memberships( $user ) : null;

			if ( ! empty( $user_memberships ) ) {

				$erase = 'yes' === get_option( 'wc_memberships_privacy_erasure_request_delete_user_memberships', 'no' );

				foreach ( $user_memberships as $user_membership ) {

					/**
					 * Filters whether to erase a user membership upon a personal data erasure request.
					 *
					 * @since 1.10.3
					 *
					 * @param bool $erase whether to erase a membership (default value set by admin setting)
					 * @param \WC_Memberships_User_Membership $user_membership the membership to erase or keep
					 */
					$erase = apply_filters( 'wc_memberships_privacy_erase_user_membership', $erase, $user_membership );

					$plan_name = $user_membership->get_plan()->get_name();

					if ( $erase && (bool) wp_delete_post( $user_membership->get_id() ) ) {
						$response['messages'][]     = sprintf( esc_html__( 'Removed User Membership for "%s".', 'woocommerce-memberships' ), $plan_name );
						$response['items_removed']  = true;
					} else {
						$response['messages'][]     = sprintf( esc_html__( 'User Membership for "%s" has been retained.', 'woocommerce-memberships' ), $plan_name );
						$response['items_retained'] = true;
					}
				}
			}
		}

		return $response;
	}


	/**
	 * Registers a GDPR compliant personal data exporter in WordPress for handling User Memberships.
	 *
	 * @internal
	 *
	 * @since 1.10.3
	 *
	 * @param array $exporters list of WordPress personal data exporters
	 * @return array
	 */
	public function register_personal_data_exporter( array $exporters ) {

		$exporters['woocommerce-memberships-user-memberships'] = array(
			'exporter_friendly_name' => __( 'User Memberships', 'woocommerce-memberships' ),
			'callback'               => array( 'WC_Memberships', 'export_personal_data' ),
		);

		return $exporters;
	}


	/**
	 * Exports personal data for a user that has issued a request to access to their stored personal information in WordPress.
	 *
	 * @internal
	 *
	 * @since 1.10.3
	 *
	 * @param string $email_address address of the user that issued the export request
	 * @return array exported data in key-value pairs
	 */
	public static function export_personal_data( $email_address ) {

		$user     = get_user_by( 'email', $email_address );
		$exported = array(
			'data' => array(),
			'done' => true,
		);

		if ( $user instanceof WP_User ) {

			$user_memberships = wc_memberships_get_user_memberships( $user );

			if ( ! empty( $user_memberships ) ) {

				foreach ( $user_memberships as $user_membership ) {

					$exported['data'][] = array(
						'group_id'    => 'woocommerce_memberships_user_memberships',
						'group_label' => __( 'Memberships', 'woocommerce-memberships' ),
						'item_id'     => "user-membership-id-{$user_membership->get_id()}",
						'data'        => self::get_user_membership_personal_data( $user_membership ),
					);
				}
			}
		}

		return $exported;
	}


	/**
	 * Gathers user membership data for personal data export.
	 *
	 * @since 1.10.3
	 *
	 * @param \WC_Memberships_User_Membership $user_membership
	 * @return array
	 */
	private static function get_user_membership_personal_data( WC_Memberships_User_Membership $user_membership ) {

		$personal_data = array(
			'membership-plan' => array(
				'name'  => __( 'Plan', 'woocommerce-memberships' ),
				'value' => $user_membership->get_plan()->get_name(),
			),
			'start-date'      => array(
				'name'  => __( 'Start Date (UTC)', 'woocommerce-memberships' ),
				'value' => $user_membership->get_start_date( 'mysql' ),
			)
		);

		if ( $end_date = $user_membership->get_end_date( 'mysql' ) ) {
			$personal_data['end-date'] = array(
				'name'  => __( 'End Date (UTC)', 'woocommerce-memberships' ),
				'value' => $end_date,
			);
		}

		if ( $order_id = $user_membership->get_order_id() ) {
			$personal_data['order'] = array(
				'name'  => __( 'Related Order', 'woocommerce-memberships' ),
				'value' => $order_id,
			);
		}

		if ( $product = $user_membership->get_product( true ) ) {
			$personal_data['product'] = array(
				'name'  => __( 'Related Product', 'woocommerce-memberships' ),
				'value' => $product->get_name(),
			);
		}

		/**
		 * Filters a User Membership's personal data to export.
		 *
		 * @since 1.10.3
		 *
		 * @param array $personal_data associative array
		 * @param \WC_Memberships_User_Membership $user_membership user membership being exported
		 */
		return (array) apply_filters( 'wc_memberships_privacy_export_user_membership_personal_data', $personal_data, $user_membership );
	}


	/** Deprecated methods ******************************************************/


	/**
	 * Backwards compatibility handler for deprecated methods.
	 *
	 * TODO remove deprecated methods when they are at least 3 minor versions older (as in x.Y.z semantic versioning) {FN 2017-06-23}
	 *
	 * @since 1.6.0
	 *
	 * @param string $method method called
	 * @param void|string|array|mixed $args optional argument(s)
	 * @return null|void|mixed
	 */
	public function __call( $method, $args ) {

		switch ( $method ) {

			/** @deprecated since 1.8.0 - remove by 1.11.0 or higher */
			case 'admin_list_post_links' :

				_deprecated_function( 'wc_memberships()->admin_list_post_links()', '1.8.0' );

				$posts = isset( $args[0] ) ? $args[0] : $args;

				if ( empty( $posts ) ) {
					return '';
				}

				$items = array();

				foreach ( $posts as $post ) {
					$items[] = '<a href="' . get_edit_post_link( $post->ID ) . '">' . get_the_title( $post->ID ) . '</a>';
				}

				return wc_memberships_list_items( $items, __( 'and', 'woocommerce-memberships' ) );

			/** @deprecated since 1.9.0 - remove by version 1.12.0 or higher */
			case 'get_query_instance' :
				_deprecated_function( 'wc_memberships()->get_query_instance()', '1.9.0' );
				return null;

		}

		// you're probably doing it wrong...
		trigger_error( 'Call to undefined method ' . __CLASS__ . '::' . $method, E_USER_ERROR );
		return null;
	}


} // end WC_Memberships class


/**
 * Returns the One True Instance of Memberships
 *
 * @since 1.0.0
 * @return WC_Memberships
 */
function wc_memberships() {
	return WC_Memberships::instance();
}

// fire it up!
wc_memberships();

} // init_woocommerce_memberships()
